//
//  NewAddEntryViewController.m
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-05.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import "AddEntryViewController.h"

#import "Trip.h"
#import "MapKitHelper.h"
#import "MapViewController.h"

/*
 * enum for each of the data entry elements in the view
 */
enum {
    NameFieldTag = 0,
    DateFieldTag = 1,
    WeatherFieldTag = 2,
    TemperatureFieldTag = 3,
    LevelMetersFieldTag = 4,
    NotesFieldTag = 5
};

@interface AddEntryViewController ()


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (assign, nonatomic) UITextView *activeField;

//// these are for passing the latitude and longitude back from the MapViewController when the user saves
//// their location
//@property (nonatomic) NSNumber *latitude;
//@property (nonatomic) NSNumber *longitude;

@end

@implementation AddEntryViewController

#pragma mark - View Life Cycle

-(void)viewDidLoad
{
    [self setViewFieldDelegates];

    // make sure we have a trip object to work on
    if (!self.trip) {
        // create a new trip
        [self setTrip:[Trip createEntity]];
    }
    
    // make the tap recognition work for another way of dismissing the keyboard
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.scrollView addGestureRecognizer:singleTap];
    
    [self registerForKeyboardNotifications];
    
}



/*
 * Here we are overriding this method to add the content size to the UIScrollView.
 * With auto layout this seems to be the only way I can make the scrolling work
 */
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scrollView layoutIfNeeded];
    self.scrollView.contentSize = CGSizeMake(320,680); // 680 is length of scrollView and contentView
}

/**
 * Use this method in the unwind segue to access the coordinates that were set in the MapViewController
 */
- (IBAction)backToAddEntryViewController:(UIStoryboardSegue *)segue
{
    if ([segue.sourceViewController isKindOfClass:[MapViewController class]]) {
        NSLog(@"from addmapviewcontroller");
        
        MapViewController *mapViewController = segue.sourceViewController;
        [mapViewController saveLocationCoordinates];
        NSLog(@"Latitude from MapViewController: %@", [mapViewController latitude]);
        NSLog(@"Latitude from MapViewController: %@", [mapViewController longitude]);
        
        // set local coordinate properties for saving to the persistent store
        [[self trip ]setLatitude:[mapViewController latitude]];
        [[self trip ]setLongitude:[mapViewController longitude]];
    }
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    //Hide keyBoard
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unregisterForKeyboardNotifications];
}

/**
 * Set all of our fields delegates to self to allow for dismissing the keyboard on return from text fields
 */
- (void)setViewFieldDelegates
{
    // set the delegates
    [[self nameField] setDelegate:self];
    [[self dateField] setDelegate:self];
    [[self weatherField] setDelegate:self];
    [[self metersField] setDelegate:self];
    [[self temperatureField] setDelegate:self];
    [[self notesField] setDelegate:self];
    [[self mapView] setDelegate:self];
}


#pragma mark - UITextFieldDelegate/UITextViewDelegate methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([[textField text] isEqualToString:@""]) {
        return;
    }
    
    NSString *inputString = [textField text];
    
    switch ([textField tag]) {
        case NameFieldTag:
            NSLog(@"Name field text entered");
            
            if (self.nameField.text.length > 0 && self.trip != nil) {
                [[self trip] setName:inputString];
            }
            break;
        case DateFieldTag:
            NSLog(@"Date field text entered");
            
            if (self.dateField.text.length > 0 && self.trip != nil) {
                [[self trip] setDate:inputString];
            }
            break;
        case WeatherFieldTag:
            NSLog(@"Weather field text entered");
            
            if (self.weatherField.text.length > 0 && self.trip != nil) {
                [[self trip] setWeather:inputString];
            }
            break;
        case TemperatureFieldTag: // Temperature is entered as an integer value into the data store
            NSLog(@"Temperature field text entered");
            
            if (self.temperatureField.text.length > 0 && self.trip != nil) {

                [[self trip] setWaterTempDegC:[NSNumber numberWithInt:[inputString intValue]]];
            }
            break;
        case LevelMetersFieldTag:  // Level is entered as a decimal value in meters
            NSLog(@"Level field text entered");
            
            if (self.metersField.text.length > 0 && self.trip != nil) {
                [[self trip] setWaterLevelMeters:[NSNumber numberWithFloat:[inputString floatValue]]];
            }
            break;

        default:
            break;
    }
    
}

/*
 * Use this to register for keyboard notifications in order to deal with they keyboard covering the bottom text view
 */
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardDidHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self setActiveField:textView];
    
}

/*
 * Grab the notes from the text view
 */
- (void)textViewDidEndEditing:(UITextView *)textView
{
    // TODO: Why is this not working?  Not even the first log is being printed
    NSLog(@"Text View did end editing");
    [textView resignFirstResponder];
    if ([textView.text length] > 0) {
        NSLog(@"Saving textView text");
        [[self trip] setNotes:textView.text];
    }
    
    [self setActiveField:nil];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    [UIView commitAnimations];
    
}

/*
 * This is fired from the notification centre so we can offset the scrollview if the keyboard is covering the current subview
 */
- (void)keyboardWasShown:(NSNotification*)aNotification {
    
    // keyboard size from the notification
    CGSize keyboardSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // move the content inset the height of the keyboard
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    
    // Only scroll the subview into place if it is covered by the keyboard
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) { 
        CGPoint scrollPoint = CGPointMake(0.0, self.activeField.frame.origin.y - (keyboardSize.height-115));
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}


/*
 * We are using the 'return' key on the keyboard to help dismiss the keyboard when pressed
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.nameField) {
        [_nameField resignFirstResponder];
    } else if (textField == self.dateField){
        [_dateField resignFirstResponder];
    } else if (textField == self.weatherField) {
        [self.weatherField resignFirstResponder];
    } else if (textField == self.temperatureField) {
        [[self temperatureField] resignFirstResponder];
    } else if (textField == self.metersField) {
        [[self metersField] resignFirstResponder];
    }

    return YES;
}

#pragma mark - MagicalRecord/CoreData interface


- (IBAction)saveTrip:(UIBarButtonItem *)sender {
    if (self.trip) {
        NSLog(@"Name: %@", [[self trip] name]);
        NSLog(@"Date: %@", [[self trip] date]);
        NSLog(@"Weather: %@", [[self trip] weather]);
        NSLog(@"Temperature: %@", [[self trip] waterTempDegC]);
        NSLog(@"Level: %@", [[self trip] waterLevelMeters]);
        NSLog(@"Notes: %@", [[self trip] notes]);
        NSLog(@"Latitude: %@", [[self trip] latitude]);
        NSLog(@"Longitude: %@", [[self trip] longitude]);
    }

        // call saveContext and hope
        [self saveContext];
    
        // clear the view which calls viewWillDisappear which in turn calls the saveContext method
    	[self.navigationController popViewControllerAnimated:YES];
}

/*
 * Since we created a trip on loading the view we have to delete it if the cancel button was pressed
 * to avoid getting empty trips in the context
 */
- (void)cancelEntry
{
    [self.trip deleteEntity];
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)mapViewButton:(UIButton *)sender {
}

- (void)saveContext {
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"You successfully saved your content");
        }
        else if (error) {
            NSLog(@"Error saving context: %@", [error localizedDescription]);
        }
    }];
}

#pragma mark - MapViewDelegate methods

/*
 * Use this delegate method to center and set the zoom level of the users location
 */
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
      [MapKitHelper setInitialZoomForCurrentLocationinMapview:[self mapView]];
}

@end
