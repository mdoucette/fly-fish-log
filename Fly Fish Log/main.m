//
//  main.m
//  Fly Fish Log
//
//  Created by Mark Doucette on 2/19/2014.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
