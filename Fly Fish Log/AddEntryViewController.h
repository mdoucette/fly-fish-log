//
//  NewAddEntryViewController.h
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-05.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class Trip;

@interface AddEntryViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, MKMapViewDelegate>

@property (nonatomic, strong) Trip *trip;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *dateField;
@property (weak, nonatomic) IBOutlet UITextField *weatherField;
@property (weak, nonatomic) IBOutlet UITextField *temperatureField;
@property (weak, nonatomic) IBOutlet UITextField *metersField;
@property (weak, nonatomic) IBOutlet UITextView *notesField;

/**
 * Save the trip to the persistent store
 *
 * @param: sender the sender of the action
 */
- (IBAction)saveTrip:(UIButton *)sender;

/**
 * Delete the trip entity if the cancel button is pressed
 */
- (void)cancelEntry;

/**
 * Use this button over top of the mapView to simulate a clickable map that opens up the full
 * MapView for allowing the user to select and save the current location
 */
- (IBAction)mapViewButton:(UIButton *)sender;
@end
