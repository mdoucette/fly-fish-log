//
//  Trip.m
//  Riffle
//
//  Created by Mark Doucette on 2014-04-02.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import "Trip.h"


@implementation Trip

@dynamic date;
@dynamic name;
@dynamic notes;
@dynamic waterLevelMeters;
@dynamic waterTempDegC;
@dynamic weather;
@dynamic latitude;
@dynamic longitude;

@end
