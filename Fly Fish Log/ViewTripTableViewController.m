//
//  FFLViewTripTableViewController.m
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-26.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import "ViewTripTableViewController.h"
#import "TripOverviewController.h"
#import "Trip.h"

@interface ViewTripTableViewController ()

@property (nonatomic, strong) NSMutableArray *trips;

@end

@implementation ViewTripTableViewController

#pragma mark - View life cycle
/*
 * Use viewWillAppear to grab our trips
 */
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // fetch the trips
    [self fetchAllTrips];
    [[self tableView] reloadData];
}

/*
 * We need to use the prepareForSegue to send our data from each cell to the FFLViewTripOverviewController
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    TripOverviewController *overview = (TripOverviewController *)[segue destinationViewController];
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    Trip *trip = self.trips[indexPath.row];
    overview.trip = trip; // pass the upcoming view our current trip
}

#pragma mark - Fetching the data
/*
 * Fetch all of the Trip data and put it to the local array
 */
- (void)fetchAllTrips
{
    // use the findall static method form MagicalRecord to grab all data
    
    self.trips = [NSMutableArray arrayWithArray:[Trip findAll]];
    NSLog(@"%lu", (unsigned long)[self.trips count]);
    // Log the trip data
    if (self.trips) {
        for (Trip *trip in self.trips) {
            if ([[trip name] length] > 0) { // only show the records that contain data
                NSLog(@"New Trip Record::");
                NSLog(@"-----------------");
                NSLog(@"Name: %@", [trip name]);
                NSLog(@"Date: %@", [trip date]);
                NSLog(@"Weather: %@", [trip weather]);
                NSLog(@"Water Temp: %@",[trip waterTempDegC]);
                NSLog(@"Water Level: %@",[trip waterLevelMeters]);
            } else {
                NSLog(@"Empty Data Entry");
            }
            
        }
    }
}

#pragma mark - UITableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.trips.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TripCell"
															forIndexPath:indexPath];
	[self configureCell:cell atIndex:indexPath];
	return cell;
}

/*
 * Use this method to do the swipe to delete functionality
 */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Trip *tripDelete = self.trips[indexPath.row];
        
        [tripDelete deleteEntity];
        [self saveContext];
        [self.trips removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

/*
 * Configure the cell with out trip name and date
 */
- (void)configureCell:(UITableViewCell*)cell atIndex:(NSIndexPath*)indexPath
{
    // Get current trip
    Trip *trip = self.trips[indexPath.row];
    cell.textLabel.text = trip.name;
    cell.detailTextLabel.text = trip.date;

}

/**
 * use this to save the state of the table when a user deletes a trip
 */
- (void)saveContext {
    [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
}
@end
