//
//  FFLMapViewController.m
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-19.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import "MapViewController.h"

#import "MapKitHelper.h"


@interface MapViewController ()



@end

@implementation MapViewController

#pragma mark - View life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set the MKMapViewDelegate
    [[self mapView] setDelegate:self];
    
    // set the zoom level as soon as the view loads. This avoids waiting for the 'update location' notification
    // which can take a while to be sent after being sent the first time on the AddEntry screen
    [MapKitHelper setInitialZoomForCurrentLocationinMapview:[self mapView]]; // call our custom zoom setter
}

/*
 * Use the prepare for seque to add the custom cancel action to the cancel button in the FFLAddEntryViewController
 * This allows us to call cancelEntry and delete the Trip without adding it to the context when the cancel button
 * is pressed
 */
//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    // we only want to set the button with the action if it is going to the FFLAddEntryViewController
////    if ([[segue identifier] isEqualToString:@"addEntry"]) {
////        
////        AddEntryViewController *nextView = [segue destinationViewController];
////        // override and customize the Cancel button
////        [[nextView navigationItem] setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
////                                                                                                      target:nextView action:@selector(cancelEntry)]];
////    }
//    [self saveLocationCoordinates];
//    
//    NSLog(@"Prepare for segue called");
//}



#pragma mark - Location saving

- (void)saveLocationCoordinates {
    CLLocation *userLocation = [[[self mapView] userLocation] location];
    if (userLocation != nil) {
        // save the coordinates to pass on
        [self setLatitude: @(userLocation.coordinate.latitude)];
        [self setLongitude: @(userLocation.coordinate.longitude)];

    }
    
    NSLog(@"Latitude: %@", [self latitude]);
    NSLog(@"Longitude: %@", [self longitude]);
}

@end
