//
//  Trip.h
//  Riffle
//
//  Created by Mark Doucette on 2014-04-02.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Trip : NSManagedObject

@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSNumber * waterLevelMeters;
@property (nonatomic, retain) NSNumber * waterTempDegC;
@property (nonatomic, retain) NSString * weather;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;

@end
