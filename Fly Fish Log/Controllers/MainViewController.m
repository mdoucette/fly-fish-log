//
//  FFLMainViewController.m
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-01.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import "MainViewController.h"

#import "AddEntryViewController.h"
#import "Trip.h"


@interface MainViewController ()


@end

@implementation MainViewController

#pragma mark - View life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}



/*
 * Use the prepare for seque to add the custom cancel action to the cancel button in the FFLAddEntryViewController
 * This allows us to call cancelEntry and delete the Trip without adding it to the context when the cancel button
 * is pressed
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // we only want to set the button with the action if it is going to the FFLAddEntryViewController
    if ([[segue identifier] isEqualToString:@"addEntry"]) {
        
        AddEntryViewController *nextView = [segue destinationViewController];
        // override and customize the Cancel button
        [[nextView navigationItem] setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                                      target:nextView action:@selector(cancelEntry)]];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
