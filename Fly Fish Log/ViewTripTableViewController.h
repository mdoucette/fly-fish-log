//
//  FFLViewTripTableViewController.h
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-26.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewTripTableViewController : UITableViewController

@end
