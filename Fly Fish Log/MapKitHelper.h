//
//  MapKitHelper.h
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-26.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapKitHelper : NSObject

/**
 * Custom zoom setting for the current location
 */
+ (void)setInitialZoomForCurrentLocationinMapview:(MKMapView *)mapView;

@end
