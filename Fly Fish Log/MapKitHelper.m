//
//  MapKitHelper.m
//  Fly Fish Log
//
//  Helper methods for working with MKMapKit
//
//  Created by Mark Doucette
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import "MapKitHelper.h"


@implementation MapKitHelper

/**
 * Custom zoom setting for the current location
 */
+ (void)setInitialZoomForCurrentLocationinMapview:(MKMapView *)mapView
{
    MKUserLocation *userLocation = mapView.userLocation;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (
                                                                  userLocation.location.coordinate, 500, 500);

    //[mapView setCenterCoordinate:mapView.userLocation.location.coordinate animated:YES]; // TODO: not centering map for some reason
    // set the region/span for the current mapView
    [mapView setRegion:[mapView regionThatFits:region] animated:YES];
}

@end
