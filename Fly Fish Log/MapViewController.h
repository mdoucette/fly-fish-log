//
//  FFLMapViewController.h
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-19.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController<MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

// these are for passing the latitude and longitude back from the MapViewController when the user saves
// their location
@property (nonatomic) NSNumber *latitude;
@property (nonatomic) NSNumber *longitude;

/**
 * Save the current location coordinates to be passed on to the AddEntryViewController then saved to the persistent store
 */
- (void)saveLocationCoordinates;

@end
