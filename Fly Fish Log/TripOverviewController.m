//
//  FFLTripOverviewController.m
//  Fly Fish Log
//
//  Created by Mark Doucette on 2014-03-26.
//  Copyright (c) 2014 Mark Doucette. All rights reserved.
//

#import "TripOverviewController.h"


@interface TripOverviewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameInfo;
@property (weak, nonatomic) IBOutlet UILabel *dateInfo;
@property (weak, nonatomic) IBOutlet UILabel *weatherInfo;
@property (weak, nonatomic) IBOutlet UILabel *waterTempInfo;
@property (weak, nonatomic) IBOutlet UILabel *waterLevelInfo;
@property (weak, nonatomic) IBOutlet UILabel *latitudeInfo;
@property (weak, nonatomic) IBOutlet UILabel *longitudeInfo;
@property (weak, nonatomic) IBOutlet UITextView *notesInfo;
@end

@implementation TripOverviewController


-(void)viewWillAppear:(BOOL)animated
{
    if (self.trip) {
        self.nameInfo.text = self.trip.name;
        self.dateInfo.text = self.trip.date;
        self.weatherInfo.text = self.trip.weather;
        int temp = [[self.trip waterTempDegC] integerValue];
        self.waterTempInfo.text = [NSString stringWithFormat:@"%d", temp];
        double waterLevel = [[self.trip waterLevelMeters] doubleValue];
        self.waterLevelInfo.text = [NSString stringWithFormat:@"%.1f", waterLevel];
        self.notesInfo.text = self.trip.notes;
        
        if ([[self trip] latitude] != nil) {
            NSString *latitudeString = [NSString stringWithFormat:@"%@", self.trip.latitude];
            
            [[self latitudeInfo] setText:latitudeString];
        }

        if ([[self trip] longitude] != nil) {
            NSString *longitudeString = [NSString stringWithFormat:@"%@", self.trip.longitude];
            
            [[self longitudeInfo] setText:longitudeString];
        }
        
        
        // Logging for good luck
        NSLog(@"Name: %@", [[self trip] name]);
        NSLog(@"Date: %@", [[self trip] date]);
        NSLog(@"Weather: %@", [[self trip] weather]);
        NSLog(@"Temperature: %@", [[self trip] waterTempDegC]);
        NSLog(@"Level: %@", [[self trip] waterLevelMeters]);
        NSLog(@"Notes: %@", [[self trip] notes]);
        NSLog(@"Latitude from Save: %@", [[self trip] latitude]);
        NSLog(@"Longitude from Save: %@", [[self trip] longitude]);
    }
}
@end
